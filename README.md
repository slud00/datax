# DataX mushrooms student project

This is a student project for the course Data-X – applied data analytics models in real world tasks (4IT439) at the University of Economics in Prague.

You can find the raw data in the data folder.<br/>
The report of our project including results is in the report.pdf file.<br/>
The best project model (tree.pkl) and other solution (random_forest.pkl) you can find in folder models.<br/>
The entire code Solution_Team_3.ipynb is in the directory notebooks.<br/>
Information about libraries, modules and packages are in requirements.txt.<br/><br/>


The rest of the information about the project can be found in the report.pdf.


